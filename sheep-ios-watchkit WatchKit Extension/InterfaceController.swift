//
//  InterfaceController.swift
//  sheep-ios-watchkit WatchKit Extension
//
//  Created by Naoki Takimura on 4/30/15.
//  Copyright (c) 2015 Team Woolmark. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
