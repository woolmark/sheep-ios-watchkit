//
//  LaunchScreenView.swift
//  sheep-ios-watchkit
//
//  Created by Naoki Takimura on 4/30/15.
//  Copyright (c) 2015 Team Woolmark. All rights reserved.
//

import UIKit

class LaunchScreenView: UIView {

    let NIB_NAME = "LaunchScreen"

    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    private func sharedInit() {
        
        if let view = NSBundle.mainBundle().loadNibNamed(NIB_NAME, owner: self, options: nil).last as? UIView {
            view.frame = self.bounds
            view.setTranslatesAutoresizingMaskIntoConstraints(true)
            view.autoresizingMask = .FlexibleHeight | .FlexibleWidth
            
            self.backgroundColor = UIColor(white: CGFloat(0), alpha: CGFloat(0))
            self.addSubview(view)
        }
        
    }
    
}

